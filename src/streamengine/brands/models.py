from django.db import models
from streamengine.models import StreamEngineBaseModel
# from events.models import Event
from django.db.models import JSONField

# Create your models here.

class Brand(StreamEngineBaseModel):
    name = models.CharField(unique=True, max_length=255)
    brand_company_legal_name = models.CharField(max_length=255, blank=True, null=True)
    address_line_1 = models.CharField(max_length=255, blank=True, null=True)
    address_line_2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    zip_code = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)

    def clean(self):
        self.name = self.name.capitalize()

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    @property
    def total_brand_assets(self):
        return self.brand_asset.count()

    @property
    def total_brand_promotional_assets(self):
        return self.brand_promotional_asset.count()

    @property
    def total_brand_OCR_assets(self):
        return self.brand_OCR_asset.count()

    @property
    def total_audio_callouts(self):
        return self.audio_callout.count()

    @property
    def total_chat_callouts(self):
        return self.chat_callout.count()

    @property
    def total_similar_words(self):
        return self.similar_word.count()

    @property
    def total_adaptation_words(self):
        return self.adaptation_word.count()

class BrandContact(StreamEngineBaseModel):
    PRIMARY_PHONE_CHOICES = (
        ('cell_phone', 'Cell Phone'),
        ('direct_phone', 'Direct Phone'),
        ('other_phone', 'Other Phone')
    )

    title = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=70, blank=True, null=True)

    cell_phone = models.CharField(max_length=255, blank=True, null=True)
    direct_phone = models.CharField(max_length=255, blank=True, null=True)
    other_phone = models.CharField(max_length=255, blank=True, null=True)
    primary_phone = models.CharField(max_length=255, blank=True, null=True, choices=PRIMARY_PHONE_CHOICES)

    city = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    zip_code = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)

    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name



class BrandAssets(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='brand_asset', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    asset_name = models.CharField(max_length=255, null=True, blank=True)
    brand_asset = models.ImageField(upload_to='brand_assets/', null=True, blank=True)
    model_trained = models.CharField(max_length=255, default="No")
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name

class BrandPromotionalAssets(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='brand_promotional_asset', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    promotional_asset_name = models.CharField(max_length=255, null=True, blank=True)
    brand_promotional_asset = models.ImageField(upload_to='brand_promotional_assets/', null=True, blank=True)
    model_trained = models.CharField(max_length=255, default="No")
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name

class BrandOCRAssets(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='brand_OCR_asset', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    ocr_asset_name = models.CharField(max_length=255, null=True, blank=True)
    brand_OCR_text = models.CharField(max_length=255, null=True, blank=True)
    brand_OCR_asset = models.ImageField(upload_to='brand_OCR_assets/', null=True, blank=True)
    model_trained = models.CharField(max_length=255, default="No")
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name

class AudioCallouts(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='audio_callout', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    audio_callout = models.CharField(max_length=255, null=True, blank=True)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name

class ChatCallouts(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='chat_callout', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    chat_callout = models.CharField(max_length=255, null=True, blank=True)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name

class SimilarWords(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='similar_word', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    similar_word = JSONField(max_length=255, null=True, blank=True)
    root_word = models.CharField(max_length=255, null=True, blank=True)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name

class AdaptationWords(StreamEngineBaseModel):
    brand = models.ForeignKey(Brand, related_name='adaptation_word', on_delete=models.CASCADE)
    # event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    adaptation_word = models.CharField(max_length=255, null=True, blank=True)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.brand.name

    def __repr__(self):
        return self.brand.name