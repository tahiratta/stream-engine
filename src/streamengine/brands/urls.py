from rest_framework import routers
from django.urls import path, include
from brands.views import (
    BrandViewSet, BrandContactViewSet,
    BrandListViewSet, BrandAssetViewSet,
    BrandPromotionalAssetViewSet, BrandOCRAssetViewSet,
    AudioCalloutViewSet, ChatCalloutViewSet,
    SimilarWordViewSet, AdaptationWordViewSet

)

router = routers.DefaultRouter()
router.register(r'brands', BrandViewSet, basename='brands')
router.register(r'brands_list', BrandListViewSet)
router.register(r'brand_contact', BrandContactViewSet)
router.register(r'brand_asset', BrandAssetViewSet)
router.register(r'brand_promotional_asset', BrandPromotionalAssetViewSet)
router.register(r'brand_ocr_asset', BrandOCRAssetViewSet)
router.register(r'audio_callout', AudioCalloutViewSet)
router.register(r'chat_callout', ChatCalloutViewSet)
router.register(r'similar_word', SimilarWordViewSet)
router.register(r'adaptation_word', AdaptationWordViewSet)


urlpatterns = [
    path('', include(router.urls))
]
