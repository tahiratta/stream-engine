from django.contrib import admin
from .models import *

admin.site.register(BrandContact)
@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand_company_legal_name')
    # add search fields
    search_fields = ['name', 'brand_company_legal_name']
admin.site.register(BrandAssets)
admin.site.register(BrandPromotionalAssets)
admin.site.register(BrandOCRAssets)
admin.site.register(AudioCallouts)
admin.site.register(ChatCallouts)
admin.site.register(SimilarWords)
admin.site.register(AdaptationWords)

