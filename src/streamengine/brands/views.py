from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

from brands.models import (
    Brand, BrandContact,
    BrandAssets, BrandPromotionalAssets,
    BrandOCRAssets, AudioCallouts,
    ChatCallouts, SimilarWords,
    AdaptationWords
)
from brands.serializers import (
    BrandSerializer, BrandListSerializer,
    BrandContactSerializer, BrandAssetsSerializer,
    BrandPromotionalAssetsSerializer, BrandOCRAssetsSerializer,
    AudioCalloutsSerializer, ChatCalloutsSerializer,
    SimilarWordsSerializer, AdaptationWordsSerializer
)

from django.db.models import Prefetch

class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.prefetch_related(Prefetch('brand_asset',
        queryset=BrandAssets.objects.order_by('-id')), Prefetch('brand_promotional_asset',
        queryset=BrandPromotionalAssets.objects.order_by('-id')), Prefetch('brandcontact_set',
        queryset=BrandContact.objects.order_by('-id')), Prefetch('brand_OCR_asset',
        queryset=BrandOCRAssets.objects.order_by('-id')), Prefetch('audio_callout',
        queryset=AudioCallouts.objects.order_by('-id')), Prefetch('chat_callout',
        queryset=ChatCallouts.objects.order_by('-id')), Prefetch('similar_word',
        queryset=SimilarWords.objects.order_by('-id')), Prefetch('adaptation_word',
        queryset=AdaptationWords.objects.order_by('-id')))
    serializer_class = BrandSerializer

    # def list(self, request, *args, **kwargs):
    #     brands = Brand.objects.all()
    #     data = []
    #     for brand in brands:
    #         brand = {
    #             'id': brand.id,
    #             'name': brand.name,
    #             'total_brand_assets': brand.total_brand_assets,
    #             'total_brand_promotional_assets': brand.total_brand_promotional_assets,
    #             'total_brand_OCR_assets': brand.total_brand_OCR_assets,
    #             'total_audio_callouts': brand.total_audio_callouts,
    #             'total_chat_callouts': brand.total_chat_callouts,
    #             'total_similar_words': brand.total_similar_words,
    #             'total_adaptation_words': brand.total_adaptation_words,
    #         }
    #         data.append(brand)
    #     return Response(data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class BrandListViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandListSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class BrandContactViewSet(viewsets.ModelViewSet):
    queryset = BrandContact.objects.all()
    serializer_class = BrandContactSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class BrandAssetViewSet(viewsets.ModelViewSet):
    queryset = BrandAssets.objects.all()
    serializer_class = BrandAssetsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class BrandPromotionalAssetViewSet(viewsets.ModelViewSet):
    queryset = BrandPromotionalAssets.objects.all()
    serializer_class = BrandPromotionalAssetsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class BrandOCRAssetViewSet(viewsets.ModelViewSet):
    queryset = BrandOCRAssets.objects.all()
    serializer_class = BrandOCRAssetsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class AudioCalloutViewSet(viewsets.ModelViewSet):
    queryset = AudioCallouts.objects.all()
    serializer_class = AudioCalloutsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class ChatCalloutViewSet(viewsets.ModelViewSet):
    queryset = ChatCallouts.objects.all()
    serializer_class = ChatCalloutsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class SimilarWordViewSet(viewsets.ModelViewSet):
    queryset = SimilarWords.objects.all()
    serializer_class = SimilarWordsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class AdaptationWordViewSet(viewsets.ModelViewSet):
    queryset = AdaptationWords.objects.all()
    serializer_class = AdaptationWordsSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)
