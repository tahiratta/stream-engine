from rest_framework import serializers
from brands.models import (
    Brand, BrandContact,
    BrandAssets, BrandOCRAssets,
    BrandPromotionalAssets, AudioCallouts,
    ChatCallouts, SimilarWords,
    AdaptationWords
)
from django.db import transaction

from custom_utilities.helpers import get_base64_image


class BrandContactSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = BrandContact
        depth = 1

        fields = ('id', 'title', 'name', 'email', 'brand',
                  'cell_phone', 'direct_phone', 'other_phone', 'primary_phone',
                  'city', 'state', 'zip_code', 'country'
                  )

    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(BrandContactSerializer, self).to_internal_value(data)

    # def create(self, validated_data):
    #     brand_id = validated_data.pop('brand_id', None)
    #     if not brand_id:
    #         return self.error_messages
    #     bc = BrandContact.objects.create(**validated_data, brand_id=brand_id)
    #     return bc


class BrandAssetsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    relative_brand_asset = serializers.SerializerMethodField(read_only=True)
    encoded_brand_asset = serializers.SerializerMethodField(read_only=True)
    id = serializers.IntegerField(required=False)

    @staticmethod
    def get_relative_brand_asset(obj):
        if obj.brand_asset:
            return obj.brand_asset.url

    @staticmethod
    def get_relative_path_brand_asset(obj):
        if obj.brand_asset:
            return obj.brand_asset.path

    @staticmethod
    def get_encoded_brand_asset(obj):
        image_path = BrandAssetsSerializer.get_relative_path_brand_asset(obj)
        return get_base64_image(image_path)

    class Meta:
        model = BrandAssets

        fields = ('id', 'asset_name', 'brand_asset', 'relative_brand_asset', 'encoded_brand_asset', 'brand',
                  'model_trained', 'default', 'created_on'
                  )
    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(BrandAssetsSerializer, self).to_internal_value(data)


class BrandPromotionalAssetsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    relative_brand_promotional_asset = serializers.SerializerMethodField(read_only=True)
    encoded_brand_promotional_asset = serializers.SerializerMethodField(read_only=True)
    id = serializers.IntegerField(required=False)

    @staticmethod
    def get_relative_brand_promotional_asset(obj):
        if obj.brand_promotional_asset:
            return obj.brand_promotional_asset.url

    @staticmethod
    def get_relative_path_brand_promotional_asset(obj):
        if obj.brand_promotional_asset:
            return obj.brand_promotional_asset.path

    @staticmethod
    def get_encoded_brand_promotional_asset(obj):
        image_path = BrandPromotionalAssetsSerializer.get_relative_path_brand_promotional_asset(obj)
        return get_base64_image(image_path)


    class Meta:
        model = BrandPromotionalAssets

        fields = ('id', 'promotional_asset_name', 'brand_promotional_asset', 'encoded_brand_promotional_asset','relative_brand_promotional_asset', 'brand',
                  'model_trained', 'default', 'created_on'
                  )

    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(BrandPromotionalAssetsSerializer, self).to_internal_value(data)

class BrandOCRAssetsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    relative_brand_OCR_asset = serializers.SerializerMethodField(read_only=True)
    encoded_brand_OCR_asset = serializers.SerializerMethodField(read_only=True)
    id = serializers.IntegerField(required=False)

    @staticmethod
    def get_relative_brand_OCR_asset(obj):
        if obj.brand_OCR_asset:
            return obj.brand_OCR_asset.url

    @staticmethod
    def get_relative_path_brand_OCR_asset(obj):
        if obj.brand_OCR_asset:
            return obj.brand_OCR_asset.path

    @staticmethod
    def get_encoded_brand_OCR_asset(obj):
        image_path = BrandOCRAssetsSerializer.get_relative_path_brand_OCR_asset(obj)
        return get_base64_image(image_path)

    class Meta:
        model = BrandOCRAssets

        fields = ('id', 'ocr_asset_name', 'brand_OCR_text', 'brand_OCR_asset', 'encoded_brand_OCR_asset','relative_brand_OCR_asset', 'brand',
                  'model_trained', 'default', 'created_on'
                  )
    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(BrandOCRAssetsSerializer, self).to_internal_value(data)


class AudioCalloutsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = AudioCallouts

        fields = ('id', 'audio_callout', 'default', 'created_on', 'brand',
                  )
    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(AudioCalloutsSerializer, self).to_internal_value(data)

class ChatCalloutsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = ChatCallouts

        fields = ('id', 'chat_callout', 'default', 'created_on', 'brand',
                  )
    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(ChatCalloutsSerializer, self).to_internal_value(data)


class SimilarWordsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = SimilarWords

        fields = ('id', 'similar_word', 'root_word', 'default', 'created_on', 'brand',
                  )
    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(SimilarWordsSerializer, self).to_internal_value(data)

    def to_representation(self, instance):
        change_fields = ('similar_word',)
        data = super().to_representation(instance)
        for field in change_fields:
            try:
                if not data[field]:
                    data[field] = []
            except KeyError:
                pass
        return data


class AdaptationWordsSerializer(serializers.ModelSerializer):
    # brand_id = serializers.IntegerField(write_only=True, required=False)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = AdaptationWords

        fields = ('id', 'adaptation_word', 'default', 'created_on', 'brand',
                  )
    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all(), required=False)
        return super(AdaptationWordsSerializer, self).to_internal_value(data)


class BrandListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        depth = 1

        fields = ('id', 'name'
                  )


class BrandSerializer(serializers.ModelSerializer):
    brand_contacts = serializers.SerializerMethodField()
    brandcontact_set = BrandContactSerializer(required=False, many=True)
    brand_asset = BrandAssetsSerializer(many=True, required=False)
    brand_promotional_asset = BrandPromotionalAssetsSerializer(many=True, required=False)
    brand_OCR_asset = BrandOCRAssetsSerializer(many=True, required=False)
    audio_callout = AudioCalloutsSerializer(many=True, required=False)
    chat_callout = ChatCalloutsSerializer(many=True, required=False)
    similar_word = SimilarWordsSerializer(many=True, required=False)
    adaptation_word = AdaptationWordsSerializer(many=True, required=False)
    # brand_logo = serializers.ImageField(required=True)

    @staticmethod
    def get_brand_contacts(obj):
        return [BrandContactSerializer(i).data for i in obj.brandcontact_set.all()]

    def create(self, validated_data):
        brand_contacts = validated_data.pop('brandcontact_set', None)
        brand_assets = validated_data.pop('brand_asset', None)
        brand_promotional_assets = validated_data.pop('brand_promotional_asset', None)
        brand_OCR_assets = validated_data.pop('brand_OCR_asset', None)
        audio_callouts = validated_data.pop('audio_callout', None)
        chat_callouts = validated_data.pop('chat_callout', None)
        similar_words = validated_data.pop('similar_word', None)
        adaptation_words = validated_data.pop('adaptation_word', None)
        brand = Brand.objects.create(**validated_data)
        if brand_contacts is not None:
            for bc in brand_contacts:
                bc['brand_id'] = brand.id
                BrandContact.objects.create(**bc)
        if brand_assets is not None:
            for brand_asset in brand_assets:
                brand_asset['brand_id'] = brand.id
                BrandAssets.objects.create(**brand_asset)
        if brand_promotional_assets is not None:
            for brand_promotional_asset in brand_promotional_assets:
                brand_promotional_asset['brand_id'] = brand.id
                BrandPromotionalAssets.objects.create(**brand_promotional_asset)
        if brand_OCR_assets is not None:
            for brand_OCR_asset in brand_OCR_assets:
                brand_OCR_asset['brand_id'] = brand.id
                BrandOCRAssets.objects.create(**brand_OCR_asset)
        if audio_callouts is not None:
            for audio_callout in audio_callouts:
                AudioCallouts.objects.create(brand=brand, **audio_callout)
        if chat_callouts is not None:
            for chat_callout in chat_callouts:
                ChatCallouts.objects.create(brand=brand, **chat_callout)
        if similar_words is not None:
            for similar_word in similar_words:
                SimilarWords.objects.create(brand=brand, **similar_word)
        if adaptation_words is not None:
            for adaptation_word in adaptation_words:
                AdaptationWords.objects.create(brand=brand, **adaptation_word)
        return brand

    class Meta:
        depth = 1
        model = Brand
        fields = ('id', 'name', 'brand_company_legal_name', #'brand_logo',
                  'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'country', 'phone', 'created_on',
                  # 'contact_phone', 'email', 'contact_phone_2', 'email_2',
                  'brand_contacts', 'brandcontact_set', 'brand_asset', 'brand_promotional_asset',
                  'brand_OCR_asset', 'audio_callout', 'chat_callout', 'similar_word', 'adaptation_word'
                  )

    def update(self, instance, validated_data):

        instance.name = validated_data.get('name', instance.name)
        instance.brand_company_legal_name = validated_data.get('brand_company_legal_name', instance.brand_company_legal_name)
        instance.address_line_1 = validated_data.get('address_line_1', instance.address_line_1)
        instance.address_line_2 = validated_data.get('address_line_2', instance.address_line_2)
        instance.city = validated_data.get('city', instance.city)
        instance.state = validated_data.get('state', instance.state)
        instance.zip_code = validated_data.get('zip_code', instance.zip_code)
        instance.country = validated_data.get('country', instance.country)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.save()

        # brandcontact_set_data = validated_data.pop('brandcontact_set', None)
        # brandcontacts = (instance.brandcontact_set).all()
        # brandcontacts = list(brandcontacts)
        #
        # if brandcontact_set_data is not None:
        #     for brandcontact in brandcontact_set_data:
        #         if brandcontacts:
        #             brandcontact_set = brandcontacts.pop(0)
        #             brandcontact_set.title = brandcontact.get('title', brandcontact_set.title)
        #             brandcontact_set.name = brandcontact.get('name', brandcontact_set.name)
        #             brandcontact_set.email = brandcontact.get('email', brandcontact_set.email)
        #             brandcontact_set.cell_phone = brandcontact.get('cell_phone', brandcontact_set.cell_phone)
        #             brandcontact_set.direct_phone = brandcontact.get('direct_phone', brandcontact_set.direct_phone)
        #             brandcontact_set.other_phone = brandcontact.get('other_phone', brandcontact_set.other_phone)
        #             brandcontact_set.primary_phone = brandcontact.get('primary_phone', brandcontact_set.primary_phone)
        #             brandcontact_set.city = brandcontact.get('city', brandcontact_set.city)
        #             brandcontact_set.state = brandcontact.get('state', brandcontact_set.state)
        #             brandcontact_set.country = brandcontact.get('country', brandcontact_set.country)
        #             brandcontact_set.zip_code = brandcontact.get('zip_code', brandcontact_set.zip_code)
        #             brandcontact_set.save()
        #         # else:
        #         #     brandcontact['brand_id'] = instance.id
        #         #     BrandContact.objects.create(**brandcontact)

        # get the nested objects list
        brandcontact_set_data = validated_data.pop('brandcontact_set', None)
        # get all nested objects related with this instance and make a dict(id, object)
        brandcontacts_data_dict = dict((i.id, i) for i in instance.brandcontact_set.all())

        if brandcontact_set_data is not None:
            for brandcontact_data in brandcontact_set_data:
                if 'id' in brandcontact_data:
                    # if exists id remove from the dict and update
                    brandcontact = brandcontacts_data_dict.pop(brandcontact_data['id'])
                    brandcontact.title = brandcontact_data.get('title', brandcontact.title)
                    brandcontact.name = brandcontact_data.get('name', brandcontact.name)
                    brandcontact.email = brandcontact_data.get('email', brandcontact.email)
                    brandcontact.cell_phone = brandcontact_data.get('cell_phone', brandcontact.cell_phone)
                    brandcontact.direct_phone = brandcontact_data.get('direct_phone', brandcontact.direct_phone)
                    brandcontact.other_phone = brandcontact_data.get('other_phone', brandcontact.other_phone)
                    brandcontact.primary_phone = brandcontact_data.get('primary_phone', brandcontact.primary_phone)
                    brandcontact.city = brandcontact_data.get('city', brandcontact.city)
                    brandcontact.state = brandcontact_data.get('state', brandcontact.state)
                    brandcontact.country = brandcontact_data.get('country', brandcontact.country)
                    brandcontact.zip_code = brandcontact_data.get('zip_code', brandcontact.zip_code)
                    brandcontact.save()
                else:
                    # else create a new object
                    BrandContact.objects.create(brand=instance, **brandcontact_data)

            # delete remaining elements because they're not present in my update call
        if len(brandcontacts_data_dict) > 0:
            for brandcontact_data_dict in brandcontacts_data_dict.values():
                brandcontact_data_dict.delete()

        # get the nested objects list
        brand_assets_data = validated_data.pop('brand_asset', None)
        # get all nested objects related with this instance and make a dict(id, object)
        brand_assets_data_dict = dict((i.id, i) for i in instance.brand_asset.all())

        if brand_assets_data is not None:
            for brand_asset_data in brand_assets_data:
                if 'id' in brand_asset_data:
                    # if exists id remove from the dict and update
                    brand_asset = brand_assets_data_dict.pop(brand_asset_data['id'])
                    brand_asset.asset_name = brand_asset_data.get('asset_name', brand_asset.asset_name)
                    brand_asset.brand_asset = brand_asset_data.get('brand_asset', brand_asset.brand_asset)
                    brand_asset.model_trained = brand_asset_data.get('model_trained', brand_asset.model_trained)
                    brand_asset.default = brand_asset_data.get('default', brand_asset.default)
                    brand_asset.save()
                else:
                    # else create a new object
                    BrandAssets.objects.create(brand=instance, **brand_asset_data)

            # delete remaining elements because they're not present in my update call
        if len(brand_assets_data_dict) > 0:
            for brand_asset_data_dict in brand_assets_data_dict.values():
                brand_asset_data_dict.delete()

        # get the nested objects list
        brand_promotional_assets_data = validated_data.pop('brand_promotional_asset', None)
        # get all nested objects related with this instance and make a dict(id, object)
        brand_promotional_assets_data_dict = dict((i.id, i) for i in instance.brand_promotional_asset.all())

        if brand_promotional_assets_data is not None:
            for brand_promotional_asset_data in brand_promotional_assets_data:
                if 'id' in brand_promotional_asset_data:
                    # if exists id remove from the dict and update
                    brand_promotional_asset = brand_promotional_assets_data_dict.pop(brand_promotional_asset_data['id'])
                    brand_promotional_asset.promotional_asset_name = brand_promotional_asset_data.get(
                        'promotional_asset_name', brand_promotional_asset.promotional_asset_name)
                    brand_promotional_asset.brand_promotional_asset = brand_promotional_asset_data.get('brand_promotional_asset', brand_promotional_asset.brand_promotional_asset)
                    brand_promotional_asset.model_trained = brand_promotional_asset_data.get('model_trained', brand_promotional_asset.model_trained)
                    brand_promotional_asset.default = brand_promotional_asset_data.get('default', brand_promotional_asset.default)
                    brand_promotional_asset.save()
                else:
                    # else create a new object
                    BrandPromotionalAssets.objects.create(brand=instance, **brand_promotional_asset_data)

            # delete remaining elements because they're not present in my update call
        if len(brand_promotional_assets_data_dict) > 0:
            for brand_promotional_asset_data_dict in brand_promotional_assets_data_dict.values():
                brand_promotional_asset_data_dict.delete()

        # get the nested objects list
        brand_OCR_assets_data = validated_data.pop('brand_OCR_asset', None)
        # get all nested objects related with this instance and make a dict(id, object)
        brand_OCR_assets_data_dict = dict((i.id, i) for i in instance.brand_OCR_asset.all())

        if brand_OCR_assets_data is not None:
            for brand_OCR_asset_data in brand_OCR_assets_data:
                if 'id' in brand_OCR_asset_data:
                    # if exists id remove from the dict and update
                    brand_OCR_asset = brand_OCR_assets_data_dict.pop(
                        brand_OCR_asset_data['id'])
                    brand_OCR_asset.ocr_asset_name = brand_OCR_asset_data.get('ocr_asset_name', brand_OCR_asset.ocr_asset_name)
                    brand_OCR_asset.brand_OCR_text = brand_OCR_asset_data.get('brand_OCR_text', brand_OCR_asset.brand_OCR_text)
                    brand_OCR_asset.brand_OCR_asset = brand_OCR_asset_data.get('brand_OCR_asset', brand_OCR_asset.brand_OCR_asset)
                    brand_OCR_asset.model_trained = brand_OCR_asset_data.get('model_trained', brand_OCR_asset.model_trained)
                    brand_OCR_asset.default = brand_OCR_asset_data.get('default', brand_OCR_asset.default)
                    brand_OCR_asset.save()
                else:
                    # else create a new object
                    BrandOCRAssets.objects.create(brand=instance, **brand_OCR_asset_data)

            # delete remaining elements because they're not present in my update call
        if len(brand_OCR_assets_data_dict) > 0:
            for brand_OCR_asset_data_dict in brand_OCR_assets_data_dict.values():
                brand_OCR_asset_data_dict.delete()

        # get the nested objects list
        audio_callouts_data = validated_data.pop('audio_callout', None)
        # get all nested objects related with this instance and make a dict(id, object)
        audio_callouts_data_dict = dict((i.id, i) for i in instance.audio_callout.all())

        if audio_callouts_data is not None:
            for audio_callout_data in audio_callouts_data:
                if 'id' in audio_callout_data:
                    # if exists id remove from the dict and update
                    audio_callout = audio_callouts_data_dict.pop(audio_callout_data['id'])
                    audio_callout.audio_callout = audio_callout_data.get('audio_callout', audio_callout.audio_callout)
                    audio_callout.default = audio_callout_data.get('default', audio_callout.default)
                    audio_callout.save()
                else:
                    # else create a new object
                    AudioCallouts.objects.create(brand=instance, **audio_callout_data)

        # delete remaining elements because they're not present in my update call
        if len(audio_callouts_data_dict) > 0:
            for audio_callout_data_dict in audio_callouts_data_dict.values():
                audio_callout_data_dict.delete()

        # get the nested objects list
        chat_callouts_data = validated_data.pop('chat_callout', None)
        # get all nested objects related with this instance and make a dict(id, object)
        chat_callouts_data_dict = dict((i.id, i) for i in instance.chat_callout.all())

        if chat_callouts_data is not None:
            for chat_callout_data in chat_callouts_data:
                if 'id' in chat_callout_data:
                    # if exists id remove from the dict and update
                    chat_callout = chat_callouts_data_dict.pop(chat_callout_data['id'])
                    chat_callout.chat_callout = chat_callout_data.get('chat_callout', chat_callout.chat_callout)
                    chat_callout.default = chat_callout_data.get('default', chat_callout.default)
                    chat_callout.save()
                else:
                    # else create a new object
                    ChatCallouts.objects.create(brand=instance, **chat_callout_data)

        # delete remaining elements because they're not present in my update call
        if len(chat_callouts_data_dict) > 0:
            for chat_callout_data_dict in chat_callouts_data_dict.values():
                chat_callout_data_dict.delete()

        # get the nested objects list
        similar_words_data = validated_data.pop('similar_word', None)
        # get all nested objects related with this instance and make a dict(id, object)
        similar_words_data_dict = dict((i.id, i) for i in instance.similar_word.all())

        if similar_words_data is not None:
            for similar_word_data in similar_words_data:
                if 'id' in similar_word_data:
                    # if exists id remove from the dict and update
                    similar_word = similar_words_data_dict.pop(similar_word_data['id'])
                    similar_word.similar_word = similar_word_data.get('similar_word', similar_word.similar_word)
                    similar_word.root_word = similar_word_data.get('root_word', similar_word.root_word)
                    similar_word.default = similar_word_data.get('default', similar_word.default)
                    similar_word.save()
                else:
                    # else create a new object
                    SimilarWords.objects.create(brand=instance, **similar_word_data)

        # delete remaining elements because they're not present in my update call
        if len(similar_words_data_dict) > 0:
            for similar_word_data_dict in similar_words_data_dict.values():
                similar_word_data_dict.delete()

        # get the nested objects list
        adaptation_words_data = validated_data.pop('adaptation_word', None)
        # get all nested objects related with this instance and make a dict(id, object)
        adaptation_words_data_dict = dict((i.id, i) for i in instance.adaptation_word.all())

        if adaptation_words_data is not None:
            for adaptation_word_data in adaptation_words_data:
                if 'id' in adaptation_word_data:
                    # if exists id remove from the dict and update
                    adaptation_word = adaptation_words_data_dict.pop(adaptation_word_data['id'])
                    adaptation_word.adaptation_word = adaptation_word_data.get('adaptation_word', adaptation_word.adaptation_word)
                    adaptation_word.default = adaptation_word_data.get('default', adaptation_word.default)
                    adaptation_word.save()
                else:
                    # else create a new object
                    AdaptationWords.objects.create(brand=instance, **adaptation_word_data)

        # delete remaining elements because they're not present in my update call
        if len(adaptation_words_data_dict) > 0:
            for adaptation_word_data_dict in adaptation_words_data_dict.values():
                adaptation_word_data_dict.delete()

        return instance