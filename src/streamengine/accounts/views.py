from rest_framework import viewsets
from rest_framework import views
from rest_framework.response import Response

# from allauth.socialaccount.providers.twitch.views import TwitchOAuth2Adapter
# from .adapters import TwitchOAuth2Adapter
# from rest_auth.registration.views import (
#     SocialLoginView,
#     SocialConnectView
# )

# from django.contrib.auth.models import Group

from accounts.models import User, UserProfile
from accounts.serializers import UserSerializer, UserProfileSerializer

# from accounts.permissions import IsLoggedInUserOrAdmin, IsAdminUser


class UserViewSet(viewsets.ModelViewSet):
    # queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        if self.request.GET.get('users') == 'company_users':
            queryset = User.objects.filter(is_client=False)
        elif self.request.GET.get('users') == 'clients':
            queryset = User.objects.filter(is_client=True)
        else:
            queryset = User.objects.all()
        return queryset

    # def get_permissions(self):
    #     permission_classes = []
    #     if self.action == 'create':
    #         permission_classes = [IsAdminUser]
    #     elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
    #         permission_classes = [IsLoggedInUserOrAdmin]
    #     elif self.action == 'list' or self.action == 'destroy':
    #         permission_classes = [IsAdminUser]
    #     return [permission() for permission in permission_classes]


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    # def get_permissions(self):
    #     permission_classes = []
    #     if self.action == 'create':
    #         permission_classes = [IsAdminUser]
    #     elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
    #         permission_classes = [IsLoggedInUserOrAdmin]
    #     elif self.action == 'list' or self.action == 'destroy':
    #         permission_classes = [IsAdminUser]
    #     return [permission() for permission in permission_classes]

# class TwitchLogin(SocialLoginView):
#     adapter_class = TwitchOAuth2Adapter
#
#
# class TwitchConnect(SocialConnectView):
#     adapter_class = TwitchOAuth2Adapter