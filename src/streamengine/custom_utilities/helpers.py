import base64
from datetime import timedelta, datetime
import time

def get_base64_image(path):
    if path:
        with open(path, 'rb') as image_obj:
            return base64.b64encode(image_obj.read()).decode("ascii")

def weekday_count(firstDay, lastDay, weekDay):
    # firstDay = datetime.strptime(firstDay, '%Y-%m-%d %H:%M:%S')
    # lastDay = datetime.strptime(lastDay, '%Y-%m-%d %H:%M:%S')
    dates = [firstDay + timedelta(days=x) for x in range((lastDay - firstDay).days + 1) if
             (firstDay + timedelta(days=x)).weekday() == time.strptime(weekDay, '%A').tm_wday]
    # for i in range((end_date - start_date).days):
    #     day = calendar.day_name[(start_date + datetime.timedelta(days=i + 1)).weekday()]
    #     week[day] = day if day in week else 1
    days = [d for d in dates]

    return days