from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Broadcaster)
admin.site.register(BigVideo)
admin.site.register(Event)
admin.site.register(EventRepeat)
admin.site.register(EventBrand)
admin.site.register(EventBrandAssets)
admin.site.register(EventBrandPromotionalAssets)
admin.site.register(EventBrandOCRAssets)
admin.site.register(EventAudioCallouts)
admin.site.register(EventChatCallouts)
admin.site.register(EventSimilarWords)
admin.site.register(EventAdaptationWords)
admin.site.register(DetectedObjects)
admin.site.register(DetectedPromotionalObjects)
admin.site.register(DetectedOCRObjects)
admin.site.register(DetectedAudioCallouts)
