from django.db import models
from streamengine.models import StreamEngineBaseModel
from accounts.models import User
# from django.contrib.postgres.fields import JSONField
from django.db.models import JSONField
from brands.models import (
    Brand,
    BrandAssets, BrandOCRAssets,
    BrandPromotionalAssets, AudioCallouts,
    ChatCallouts, SimilarWords,
    AdaptationWords
)
import pytz

# Create your models here.
class Broadcaster(StreamEngineBaseModel):
    broadcaster = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.broadcaster

    def __repr__(self):
        return self.broadcaster

class BigVideo(StreamEngineBaseModel):
    big_video = models.FileField(upload_to='big_videos/', null=True, blank=True)
    # celery_task_id = models.CharField(max_length=200, blank=True, null=True)
    # upload_status = models.CharField(max_length=200, default="in_process")

    def __int__(self):
        return self.id

    def __repr__(self):
        return self.id

class Event(StreamEngineBaseModel):
    event_name = models.CharField(unique=True, max_length=255)
    channel_name = models.CharField(max_length=255, null=True, blank=True)
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name='client')
    broadcaster = models.ForeignKey(Broadcaster, on_delete=models.CASCADE)
    upload_video = models.FileField(upload_to='event_videos/', null=True, blank=True)
    video_name = models.CharField(max_length=255, null=True, blank=True)
    stream_url = models.CharField(max_length=255, null=True, blank=True)
    time_zone = models.CharField(max_length=255, null=True, blank=True)
    start_now = models.BooleanField(default=False)
    start_later = models.BooleanField(default=True)
    nrt = models.BooleanField(default=False)
    offline = models.BooleanField(default=True)
    are_brand_assets = models.BooleanField(default=True)
    are_promotional_assets = models.BooleanField(default=True)
    are_OCR_assets = models.BooleanField(default=True)
    are_audio_callouts = models.BooleanField(default=True)
    are_chat_callouts = models.BooleanField(default=True)
    #TODO Remove all repeat related fields except repeat and repeat weekly
    event_start_datetime = models.DateTimeField(null=True, blank=True)
    event_end_datetime = models.DateTimeField(null=True, blank=True)
    event_duration = models.CharField(max_length=255, null=True, blank=True)
    event_status = models.CharField(max_length=255, default='upcoming')
    repeat = models.BooleanField(default=False)
    repeat_days = JSONField(max_length=225, null=True, blank=True)
    repeat_weekly = models.BooleanField(default=False)
    until_datetime = models.DateTimeField(null=True, blank=True)
    enabled = models.BooleanField(default=True)

    # def save(self, *args, **kwargs):
    #     tz = pytz.timezone(self.time_zone)
    #     self.event_start_datetime = self.event_start_datetime.replace(tzinfo=None)
    #     self.event_start_datetime = tz.localize(self.event_start_datetime)
    #
    #     self.event_end_datetime = self.event_end_datetime.replace(tzinfo=None)
    #     self.event_end_datetime = tz.localize(self.event_end_datetime)
    #     print('start time is:', self.event_start_datetime, 'end time is:', self.event_end_datetime)
    #     super().save(*args, **kwargs)

    def __str__(self):
        return self.event_name

    def __repr__(self):
        return self.event_name

    @property
    def no_of_brands(self):
        return self.event_brand.count()

class EventRepeat(StreamEngineBaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="eventrepeat_set")
    event_start_datetime = models.DateTimeField(null=True, blank=True)
    event_end_datetime = models.DateTimeField(null=True, blank=True)
    event_duration = models.CharField(max_length=255, null=True, blank=True)
    event_status = models.CharField(max_length=255, default='upcoming')
    # repeat = models.BooleanField(default=False)
    repeat_day = models.CharField(max_length=225, null=True, blank=True)
    until_datetime = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.event.event_name + " " + str(self.event_start_datetime)

    def __repr__(self):
        return self.event.event_name + " " + str(self.event_start_datetime)

class EventBrand(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_brand', on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventBrandAssets(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_brand_asset', on_delete=models.CASCADE)
    brand_asset = models.ForeignKey(BrandAssets, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventBrandPromotionalAssets(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_brand_promotional_asset', on_delete=models.CASCADE)
    brand_promotional_asset = models.ForeignKey(BrandPromotionalAssets, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventBrandOCRAssets(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_brand_OCR_asset', on_delete=models.CASCADE)
    brand_OCR_asset = models.ForeignKey(BrandOCRAssets, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventAudioCallouts(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_audio_callout', on_delete=models.CASCADE)
    audio_callout = models.ForeignKey(AudioCallouts, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventChatCallouts(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_chat_callout', on_delete=models.CASCADE)
    chat_callout = models.ForeignKey(ChatCallouts, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventSimilarWords(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_similar_word', on_delete=models.CASCADE)
    similar_word = models.ForeignKey(SimilarWords, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class EventAdaptationWords(StreamEngineBaseModel):
    event = models.ForeignKey(Event, related_name='event_adaptation_word', on_delete=models.CASCADE)
    adaptation_word = models.ForeignKey(AdaptationWords, on_delete=models.CASCADE)

    def __str__(self):
        return self.event.event_name

    def __repr__(self):
        return self.event.event_name

class DetectedObjects(StreamEngineBaseModel):
    # TODO add event forignkey to relate detectedObjects with event model
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    frame_number = models.IntegerField(null=True, blank=True)
    brand_name = models.CharField(max_length=255, null=True, blank=True)
    object_x_centre = models.FloatField(null=True, blank=True)
    object_y_centre = models.FloatField(null=True, blank=True)
    object_width = models.FloatField(null=True, blank=True)
    object_height = models.FloatField(null=True, blank=True)
    percentage_probability = models.FloatField(null=True, blank=True)
    pts_time = models.FloatField(null=True, blank=True)

class DetectedPromotionalObjects(StreamEngineBaseModel):
    # TODO add event forignkey to relate detectedObjects with event model
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    frame_number = models.IntegerField(null=True, blank=True)
    # brand_name = models.CharField(max_length=255, null=True, blank=True)
    # TODO change promo_accet integerfield with forignkey
    brand_promotional_asset = models.ForeignKey(BrandPromotionalAssets, on_delete=models.CASCADE, null=True, blank=True)
    object_x_centre = models.FloatField(null=True, blank=True)
    object_y_centre = models.FloatField(null=True, blank=True)
    object_width = models.FloatField(null=True, blank=True)
    object_height = models.FloatField(null=True, blank=True)
    percentage_probability = models.FloatField(null=True, blank=True)
    pts_time = models.FloatField(null=True, blank=True)

class DetectedOCRObjects(StreamEngineBaseModel):
    # TODO add event forignkey to relate detectedObjects with event model
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    frame_number = models.IntegerField(null=True, blank=True)
    # brand_name = models.CharField(max_length=255, null=True, blank=True)
    # TODO change ocr_asset integerfield with forignkey
    brand_ocr_asset = models.ForeignKey(BrandOCRAssets, on_delete=models.CASCADE, null=True, blank=True)
    object_x_centre = models.FloatField(null=True, blank=True)
    detected_text = models.TextField(null=True, blank=True)
    object_y_centre = models.FloatField(null=True, blank=True)
    object_width = models.FloatField(null=True, blank=True)
    object_height = models.FloatField(null=True, blank=True)
    percentage_probability = models.FloatField(null=True, blank=True)
    pts_time = models.FloatField(null=True, blank=True)

class DetectedAudioCallouts(StreamEngineBaseModel):
    # TODO add event forignkey to relate detectedObjects with event model
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)
    sentence_start_time = models.TimeField(null=True, blank=True)
    sentence = models.CharField(max_length=255, null=True, blank=True)
    confidence = models.FloatField(max_length=255, null=True, blank=True)
    word_list = JSONField(null=True, blank=True)


class Model(StreamEngineBaseModel):
    model_id = models.IntegerField(unique=True)
    production_model = models.BooleanField(default=False)


class ModelBrandAssetsRelation(StreamEngineBaseModel):
    model_id = models.ForeignKey(Model, on_delete=models.CASCADE, null=True, blank=True)
    brand_id = models.ForeignKey(Brand, on_delete=models.CASCADE, null=True, blank=True)
    brand_asset_id = models.ForeignKey(BrandAssets, on_delete=models.CASCADE, null=True, blank=True)
    brand_promotional_asset_id = models.ForeignKey(BrandPromotionalAssets, on_delete=models.CASCADE, null=True, blank=True)




