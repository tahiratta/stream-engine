# Generated by Django 3.1 on 2020-10-23 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0031_remove_eventrepeat_repeat'),
    ]

    operations = [
        migrations.AddField(
            model_name='bigvideo',
            name='celery_task_id',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='bigvideo',
            name='upload_status',
            field=models.CharField(default='in_process', max_length=200),
        ),
    ]
