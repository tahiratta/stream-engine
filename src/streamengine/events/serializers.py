from rest_framework import serializers
from events.models import (
    DetectedObjects, DetectedPromotionalObjects,
    DetectedOCRObjects, Broadcaster,
    EventBrand, EventBrandAssets,
    EventBrandPromotionalAssets, EventBrandOCRAssets,
    EventAudioCallouts, EventChatCallouts,
    EventSimilarWords, EventAdaptationWords,
    DetectedAudioCallouts, Event,
    EventRepeat, BigVideo

)
from accounts.models import User
from brands.models import (
    Brand, BrandAssets,
    BrandPromotionalAssets, BrandOCRAssets,
    AudioCallouts, ChatCallouts,
    SimilarWords, AdaptationWords
)
from hurry.filesize import size, si
from custom_utilities.helpers import weekday_count

class BroadcasterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Broadcaster
        fields = (
            'id', 'broadcaster'
        )

class EventBrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventBrand
        depth = 1

        fields = ('id', 'brand'
                  )

    def to_internal_value(self, data):
        self.fields['brand'] = serializers.PrimaryKeyRelatedField(
            queryset=Brand.objects.all())
        return super(EventBrandSerializer, self).to_internal_value(data)

class EventBrandAssetsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventBrandAssets
        depth = 1

        fields = ('id', 'brand_asset'
                  )

    def to_internal_value(self, data):
        self.fields['brand_asset'] = serializers.PrimaryKeyRelatedField(
            queryset=BrandAssets.objects.all())
        return super(EventBrandAssetsSerializer, self).to_internal_value(data)


class EventBrandPromotionalAssetsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventBrandPromotionalAssets
        depth = 1

        fields = ('id', 'brand_promotional_asset'
                  )

    def to_internal_value(self, data):
        self.fields['brand_promotional_asset'] = serializers.PrimaryKeyRelatedField(
            queryset=BrandPromotionalAssets.objects.all())
        return super(EventBrandPromotionalAssetsSerializer, self).to_internal_value(data)

class EventBrandOCRAssetsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventBrandOCRAssets
        depth = 1

        fields = ('id', 'brand_OCR_asset'
                  )

    def to_internal_value(self, data):
        self.fields['brand_OCR_asset'] = serializers.PrimaryKeyRelatedField(
            queryset=BrandOCRAssets.objects.all())
        return super(EventBrandOCRAssetsSerializer, self).to_internal_value(data)

class EventAudioCalloutsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventAudioCallouts
        depth = 1

        fields = ('id', 'audio_callout'
                  )

    def to_internal_value(self, data):
        self.fields['audio_callout'] = serializers.PrimaryKeyRelatedField(
            queryset=AudioCallouts.objects.all())
        return super(EventAudioCalloutsSerializer, self).to_internal_value(data)

class EventChatCalloutsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventChatCallouts
        depth = 1

        fields = ('id', 'chat_callout'
                  )

    def to_internal_value(self, data):
        self.fields['chat_callout'] = serializers.PrimaryKeyRelatedField(
            queryset=ChatCallouts.objects.all())
        return super(EventChatCalloutsSerializer, self).to_internal_value(data)

class EventSimilarWordsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventSimilarWords
        depth = 1

        fields = ('id', 'similar_word'
                  )

    def to_internal_value(self, data):
        self.fields['similar_word'] = serializers.PrimaryKeyRelatedField(
            queryset=SimilarWords.objects.all())
        return super(EventSimilarWordsSerializer, self).to_internal_value(data)

class EventAdaptationWordsSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventAdaptationWords
        depth = 1

        fields = ('id', 'adaptation_word'
                  )

    def to_internal_value(self, data):
        self.fields['adaptation_word'] = serializers.PrimaryKeyRelatedField(
            queryset=AdaptationWords.objects.all())
        return super(EventAdaptationWordsSerializer, self).to_internal_value(data)

class EventRepeatSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventRepeat
        # depth = 1

        fields = ('id', 'event_start_datetime', 'event_end_datetime', 'event_duration', 'event_status',
                  'repeat_day', 'until_datetime'
                  )

from django.utils.timezone import is_aware, is_naive
class EventSerializer(serializers.ModelSerializer):

    broadcaster_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    no_of_brands = serializers.SerializerMethodField()
    event_brand = EventBrandSerializer(many=True, required=False)
    event_brand_asset = EventBrandAssetsSerializer(many=True, required=False)
    event_brand_promotional_asset = EventBrandPromotionalAssetsSerializer(many=True, required=False)
    event_brand_OCR_asset = EventBrandOCRAssetsSerializer(many=True, required=False)
    event_audio_callout = EventAudioCalloutsSerializer(many=True, required=False)
    event_chat_callout = EventChatCalloutsSerializer(many=True, required=False)
    event_similar_word = EventSimilarWordsSerializer(many=True, required=False)
    event_adaptation_word = EventAdaptationWordsSerializer(many=True, required=False)
    eventrepeat_set = EventRepeatSerializer(many=True, required=False)

    @staticmethod
    def get_broadcaster_name(obj):
        return obj.broadcaster.broadcaster

    @staticmethod
    def get_client_name(obj):
        return obj.client.username

    @staticmethod
    def get_no_of_brands(obj):
        return obj.no_of_brands


    def create(self, validated_data):
        event_brands = validated_data.pop('event_brand', None)
        event_brand_assets = validated_data.pop('event_brand_asset', None)
        event_brand_promotional_assets = validated_data.pop('event_brand_promotional_asset', None)
        event_brand_OCR_assets = validated_data.pop('event_brand_OCR_asset', None)
        event_audio_callouts = validated_data.pop('event_audio_callout', None)
        event_chat_callouts = validated_data.pop('event_chat_callout', None)
        event_similar_words = validated_data.pop('event_similar_word', None)
        event_adaptation_words = validated_data.pop('event_adaptation_word', None)
        event_repeats = validated_data.pop('eventrepeat_set', None)
        event = Event.objects.create(**validated_data)
        if event_brands is not None:
            for event_brand in event_brands:
                event_brand['event_id'] = event.id
                EventBrand.objects.create(**event_brand)
        if event_brand_assets is not None:
            for event_brand_asset in event_brand_assets:
                event_brand_asset['event_id'] = event.id
                EventBrandAssets.objects.create(**event_brand_asset)
        if event_brand_promotional_assets is not None:
            for event_brand_promotional_asset in event_brand_promotional_assets:
                event_brand_promotional_asset['event_id'] = event.id
                EventBrandPromotionalAssets.objects.create(**event_brand_promotional_asset)
        if event_brand_OCR_assets is not None:
            for event_brand_OCR_asset in event_brand_OCR_assets:
                event_brand_OCR_asset['event_id'] = event.id
                EventBrandOCRAssets.objects.create(**event_brand_OCR_asset)
        if event_audio_callouts is not None:
            for event_audio_callout in event_audio_callouts:
                EventAudioCallouts.objects.create(event=event, **event_audio_callout)
        if event_chat_callouts is not None:
            for event_chat_callout in event_chat_callouts:
                EventChatCallouts.objects.create(event=event, **event_chat_callout)
        if event_similar_words is not None:
            for event_similar_word in event_similar_words:
                EventSimilarWords.objects.create(event=event, **event_similar_word)
        if event_adaptation_words is not None:
            for event_adaptation_word in event_adaptation_words:
                EventAdaptationWords.objects.create(event=event, **event_adaptation_word)
        if event_repeats is not None:
            for event_repeat in event_repeats:
                event_start_datetime = event_repeat.get('event_start_datetime', None)
                event_end_datetime = event_repeat.get('event_end_datetime', None)
                event_duration = event_repeat.get('event_duration', None)
                event_status = event_repeat.get('event_status', 'upcoming')
                repeat_day = event_repeat.get('repeat_day', None)
                until_datetime = event_repeat.get('until_datetime', None)

                start_datetime = weekday_count(event_start_datetime, until_datetime, repeat_day)
                end_datetime = weekday_count(event_end_datetime, until_datetime, repeat_day)
                for start, end in zip(start_datetime, end_datetime):
                    EventRepeat.objects.create(event=event, event_start_datetime=start,
                                               event_end_datetime=end, event_duration=event_duration,
                                               event_status=event_status, repeat_day=repeat_day, until_datetime=until_datetime,
                                               )
        return event

    def update(self, instance, validated_data):

        instance.event_name = validated_data.get('event_name', instance.event_name)
        instance.channel_name = validated_data.get('channel_name', instance.channel_name)
        instance.client = validated_data.get('client', instance.client)
        instance.broadcaster = validated_data.get('broadcaster', instance.broadcaster)
        instance.upload_video = validated_data.get('upload_video', instance.upload_video)
        instance.video_name = validated_data.get('video_name', instance.video_name)
        instance.stream_url = validated_data.get('stream_url', instance.stream_url)
        instance.start_now = validated_data.get('start_now', instance.start_now)
        instance.start_later = validated_data.get('start_later', instance.start_later)
        instance.nrt = validated_data.get('nrt', instance.nrt)
        instance.offline = validated_data.get('offline', instance.offline)
        instance.are_brand_assets = validated_data.get('are_brand_assets', instance.are_brand_assets)
        instance.are_promotional_assets = validated_data.get('are_promotional_assets', instance.are_promotional_assets)
        instance.are_OCR_assets = validated_data.get('are_OCR_assets', instance.are_OCR_assets)
        instance.are_audio_callouts = validated_data.get('are_audio_callouts', instance.are_audio_callouts)
        instance.are_chat_callouts = validated_data.get('are_chat_callouts', instance.are_chat_callouts)
        instance.time_zone = validated_data.get('time_zone', instance.time_zone)
        instance.event_start_datetime = validated_data.get('event_start_datetime', instance.event_start_datetime)
        instance.event_end_datetime = validated_data.get('event_end_datetime', instance.event_end_datetime)
        instance.event_duration = validated_data.get('event_duration', instance.event_duration)
        instance.repeat = validated_data.get('repeat', instance.repeat)
        instance.repeat_days = validated_data.get('repeat_days', instance.repeat_days)
        instance.repeat_weekly = validated_data.get('repeat_weekly', instance.repeat_weekly)
        # instance.repeat_monthly = validated_data.get('repeat_monthly', instance.repeat_monthly)
        instance.until_datetime = validated_data.get('until_datetime', instance.until_datetime)
        instance.enabled = validated_data.get('enabled', instance.enabled)
        instance.save()

        # get the nested objects list
        event_brands_data = validated_data.pop('event_brand', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_brands_data_dict = dict((i.id, i) for i in instance.event_brand.all())

        if event_brands_data is not None:
            for event_brand_data in event_brands_data:
                if 'id' in event_brand_data:
                    # if exists id remove from the dict and update
                    event_brand = event_brands_data_dict.pop(event_brand_data['id'])
                    event_brand.brand = event_brand_data.get('brand', event_brand.brand)
                    event_brand.save()
                else:
                    # else create a new object
                    EventBrand.objects.create(event=instance, **event_brand_data)

            # delete remaining elements because they're not present in my update call
        if len(event_brands_data_dict) > 0:
            for event_brand_data_dict in event_brands_data_dict.values():
                event_brand_data_dict.delete()

        # get the nested objects list
        event_brand_assets_data = validated_data.pop('event_brand_asset', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_brand_assets_data_dict = dict((i.id, i) for i in instance.event_brand_asset.all())

        if event_brand_assets_data is not None:
            for event_brand_asset_data in event_brand_assets_data:
                if 'id' in event_brand_asset_data:
                    # if exists id remove from the dict and update
                    event_brand_asset = event_brand_assets_data_dict.pop(event_brand_asset_data['id'])
                    event_brand_asset.brand_asset = event_brand_asset_data.get('brand_asset', event_brand_asset.brand_asset)
                    event_brand_asset.save()
                else:
                    # else create a new object
                    EventBrandAssets.objects.create(event=instance, **event_brand_asset_data)

            # delete remaining elements because they're not present in my update call
        if len(event_brand_assets_data_dict) > 0:
            for event_brand_asset_data_dict in event_brand_assets_data_dict.values():
                event_brand_asset_data_dict.delete()

        # get the nested objects list
        event_brand_promotional_assets_data = validated_data.pop('event_brand_promotional_asset', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_brand_promotional_assets_data_dict = dict((i.id, i) for i in instance.event_brand_promotional_asset.all())

        if event_brand_promotional_assets_data is not None:
            for event_brand_promotional_asset_data in event_brand_promotional_assets_data:
                if 'id' in event_brand_promotional_asset_data:
                    # if exists id remove from the dict and update
                    event_brand_promotional_asset = event_brand_promotional_assets_data_dict.pop(event_brand_promotional_asset_data['id'])
                    event_brand_promotional_asset.brand_promotional_asset = event_brand_promotional_asset_data.get('brand_promotional_asset', event_brand_promotional_asset.brand_promotional_asset)
                    event_brand_promotional_asset.save()
                else:
                    # else create a new object
                    EventBrandPromotionalAssets.objects.create(event=instance, **event_brand_promotional_asset_data)

            # delete remaining elements because they're not present in my update call
        if len(event_brand_promotional_assets_data_dict) > 0:
            for event_brand_promotional_asset_data_dict in event_brand_promotional_assets_data_dict.values():
                event_brand_promotional_asset_data_dict.delete()

        # get the nested objects list
        event_brand_OCR_assets_data = validated_data.pop('event_brand_OCR_asset', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_brand_OCR_assets_data_dict = dict((i.id, i) for i in instance.event_brand_OCR_asset.all())

        if event_brand_OCR_assets_data is not None:
            for event_brand_OCR_asset_data in event_brand_OCR_assets_data:
                if 'id' in event_brand_OCR_asset_data:
                    # if exists id remove from the dict and update
                    event_brand_OCR_asset = event_brand_OCR_assets_data_dict.pop(event_brand_OCR_asset_data['id'])
                    event_brand_OCR_asset.brand_OCR_asset = event_brand_OCR_asset_data.get('brand_OCR_asset', event_brand_OCR_asset.brand_OCR_asset)
                    event_brand_OCR_asset.save()
                else:
                    # else create a new object
                    EventBrandOCRAssets.objects.create(event=instance, **event_brand_OCR_asset_data)

            # delete remaining elements because they're not present in my update call
        if len(event_brand_OCR_assets_data_dict) > 0:
            for event_brand_OCR_asset_data_dict in event_brand_OCR_assets_data_dict.values():
                event_brand_OCR_asset_data_dict.delete()

        # get the nested objects list
        event_audio_callouts_data = validated_data.pop('event_audio_callout', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_audio_callouts_data_dict = dict((i.id, i) for i in instance.event_audio_callout.all())

        if event_audio_callouts_data is not None:
            for event_audio_callout_data in event_audio_callouts_data:
                if 'id' in event_audio_callout_data:
                    # if exists id remove from the dict and update
                    event_audio_callout = event_audio_callouts_data_dict.pop(event_audio_callout_data['id'])
                    event_audio_callout.audio_callout = event_audio_callout_data.get('audio_callout', event_audio_callout.audio_callout)
                    event_audio_callout.save()
                else:
                    # else create a new object
                    EventAudioCallouts.objects.create(event=instance, **event_audio_callout_data)

            # delete remaining elements because they're not present in my update call
        if len(event_audio_callouts_data_dict) > 0:
            for event_audio_callout_data_dict in event_audio_callouts_data_dict.values():
                event_audio_callout_data_dict.delete()

        # get the nested objects list
        event_chat_callouts_data = validated_data.pop('event_chat_callout', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_chat_callouts_data_dict = dict((i.id, i) for i in instance.event_chat_callout.all())

        if event_chat_callouts_data is not None:
            for event_chat_callout_data in event_chat_callouts_data:
                if 'id' in event_chat_callout_data:
                    # if exists id remove from the dict and update
                    event_chat_callout = event_chat_callouts_data_dict.pop(event_chat_callout_data['id'])
                    event_chat_callout.chat_callout = event_chat_callout_data.get('chat_callout', event_chat_callout.chat_callout)
                    event_chat_callout.save()
                else:
                    # else create a new object
                    EventChatCallouts.objects.create(event=instance, **event_chat_callout_data)

            # delete remaining elements because they're not present in my update call
        if len(event_chat_callouts_data_dict) > 0:
            for event_chat_callout_data_dict in event_chat_callouts_data_dict.values():
                event_chat_callout_data_dict.delete()

        # get the nested objects list
        event_similar_words_data = validated_data.pop('event_similar_word', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_similar_words_data_dict = dict((i.id, i) for i in instance.event_similar_word.all())

        if event_similar_words_data is not None:
            for event_similar_word_data in event_similar_words_data:
                if 'id' in event_similar_word_data:
                    # if exists id remove from the dict and update
                    event_similar_word = event_similar_words_data_dict.pop(event_similar_word_data['id'])
                    event_similar_word.similar_word = event_similar_word_data.get('similar_word', event_similar_word.similar_word)
                    event_similar_word.save()
                else:
                    # else create a new object
                    EventSimilarWords.objects.create(event=instance, **event_similar_word_data)

            # delete remaining elements because they're not present in my update call
        if len(event_similar_words_data_dict) > 0:
            for event_similar_word_data_dict in event_similar_words_data_dict.values():
                event_similar_word_data_dict.delete()

        # get the nested objects list
        event_adaptation_words_data = validated_data.pop('event_adaptation_word', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_adaptation_words_data_dict = dict((i.id, i) for i in instance.event_adaptation_word.all())

        if event_adaptation_words_data is not None:
            for event_adaptation_word_data in event_adaptation_words_data:
                if 'id' in event_adaptation_word_data:
                    # if exists id remove from the dict and update
                    event_adaptation_word = event_adaptation_words_data_dict.pop(event_adaptation_word_data['id'])
                    event_adaptation_word.adaptation_word = event_adaptation_word_data.get('adaptation_word', event_adaptation_word.adaptation_word)
                    event_adaptation_word.save()
                else:
                    # else create a new object
                    EventAdaptationWords.objects.create(event=instance, **event_adaptation_word_data)

            # delete remaining elements because they're not present in my update call
        if len(event_adaptation_words_data_dict) > 0:
            for event_adaptation_word_data_dict in event_adaptation_words_data_dict.values():
                event_adaptation_word_data_dict.delete()

        # get the nested objects list
        event_repeats_data = validated_data.pop('eventrepeat_set', None)
        # get all nested objects related with this instance and make a dict(id, object)
        event_repeats_data_dict = dict((i.id, i) for i in instance.eventrepeat_set.all())

        if event_repeats_data is not None:
            for event_repeat_data in event_repeats_data:
                if 'id' in event_repeat_data:
                    # if exists id remove from the dict and update
                    event_repeat = event_repeats_data_dict.pop(event_repeat_data['id'])
                    event_repeat.event_start_datetime = event_repeat_data.get('event_start_datetime',
                                                                         event_repeat.event_start_datetime)
                    event_repeat.event_end_datetime = event_repeat_data.get('event_end_datetime',
                                                                              event_repeat.event_end_datetime)
                    event_repeat.event_duration = event_repeat_data.get('event_duration',
                                                                              event_repeat.event_duration)
                    repeat_day = event_repeat.repeat_day
                    event_repeat.repeat_day = event_repeat_data.get('repeat_day',
                                                                              event_repeat.repeat_day)
                    event_repeat.until_datetime = event_repeat_data.get('until_datetime',
                                                                              event_repeat.until_datetime)

                    if event_repeat_data.get('repeat_day')!=event_repeat.repeat_day:
                        EventRepeat.objects.filter(repeat_day=repeat_day).delete()
                        start_datetime = weekday_count(event_repeat.event_start_datetime, event_repeat.until_datetime, event_repeat.repeat_day)
                        end_datetime = weekday_count(event_repeat.event_end_datetime, event_repeat.until_datetime, event_repeat.repeat_day)
                        for start, end in zip(start_datetime, end_datetime):
                            EventRepeat.objects.create(event=instance, event_start_datetime=start,
                                                       event_end_datetime=end, event_duration=event_duration,
                                                       repeat_day=event_repeat.repeat_day,
                                                       until_datetime=event_repeat.until_datetime,
                                                       )

                    else:
                        event_repeat.save()
                else:
                    # else find weekdays against the repeatday to until_datetime and create new objects
                    event_start_datetime = event_repeat_data.get('event_start_datetime', None)
                    event_end_datetime = event_repeat_data.get('event_end_datetime', None)
                    event_duration = event_repeat_data.get('event_duration', None)
                    event_status = event_repeat_data.get('event_status', 'upcoming')
                    repeat_day = event_repeat_data.get('repeat_day', None)
                    until_datetime = event_repeat_data.get('until_datetime', None)

                    start_datetime = weekday_count(event_start_datetime, until_datetime, repeat_day)
                    end_datetime = weekday_count(event_end_datetime, until_datetime, repeat_day)
                    for start, end in zip(start_datetime, end_datetime):
                        EventRepeat.objects.create(event=instance, event_start_datetime=start,
                                                   event_end_datetime=end, event_duration=event_duration,
                                                   event_status=event_status, repeat_day=repeat_day,
                                                   until_datetime=until_datetime,
                                                   )

                    # EventRepeat.objects.create(event=instance, **event_repeat_data)

            # delete remaining elements because they're not present in my update call
        if len(event_repeats_data_dict) > 0:
            for event_repeat_data_dict in event_repeats_data_dict.values():
                event_repeat_data_dict.delete()

        return instance

    class Meta:
        depth = 1
        model = Event
        fields = ('id', 'event_name', 'channel_name', 'client', 'client_name',
                  'broadcaster', 'broadcaster_name', 'upload_video', 'video_name', 'stream_url', 'time_zone', 'start_now', 'start_later',
                  'nrt', 'offline', 'are_brand_assets', 'are_promotional_assets', 'are_OCR_assets', 'are_audio_callouts', 'are_chat_callouts',
                  'event_start_datetime', 'created_on', 'event_end_datetime', 'event_duration', 'event_status', 'repeat',
                  'repeat_days', 'repeat_weekly', 'until_datetime', 'no_of_brands',
                  'event_brand', 'event_brand_asset', 'event_brand_promotional_asset', 'event_brand_OCR_asset',
                  'event_audio_callout', 'event_chat_callout', 'event_similar_word', 'event_adaptation_word', 'eventrepeat_set', 'enabled'
                  )

    def to_internal_value(self, data):
        self.fields['broadcaster'] = serializers.PrimaryKeyRelatedField(
            queryset=Broadcaster.objects.all())
        self.fields['client'] = serializers.PrimaryKeyRelatedField(
            queryset=User.objects.all())
        return super(EventSerializer, self).to_internal_value(data)

class BigEventVideoSerializer(serializers.ModelSerializer):
    video_size = serializers.SerializerMethodField()
    video_name = serializers.SerializerMethodField()
    video_type = serializers.SerializerMethodField()

    class Meta:
        model = BigVideo
        fields = ('id', 'big_video', 'video_name', 'video_size', 'video_type', 'created_on')
    @staticmethod
    def get_video_name(obj):
        if obj.big_video:
            video_name = obj.big_video.name
            video_name = video_name.split('/')[1]
            return video_name

    @staticmethod
    def get_video_size(obj):
        if obj.big_video:
            # print('{:,.0f}'.format(event.upload_video.size / float(1 << 20)) + " MB")
            return size(obj.big_video.size, system=si)

    @staticmethod
    def get_video_type(obj):
        if obj.big_video:
            video_name = obj.big_video.name
            video_type = video_name.split('.')[1]
            return video_type

class DetectedObjectsSerializer(serializers.ModelSerializer):

    class Meta:
        model = DetectedObjects
        fields = (
            'id', 'event', 'frame_number', 'brand_name', 'object_x_centre', 'object_y_centre', 'object_width', 'object_height',
            'percentage_probability', 'pts_time')

class DetectedPromotionalObjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetectedPromotionalObjects
        fields = (
            'id', 'event', 'frame_number', 'brand_promotional_asset', 'object_x_centre', 'object_y_centre', 'object_width',
            'object_height', 'percentage_probability', 'pts_time')

class DetectedOCRObjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetectedOCRObjects
        fields = (
            'id', 'event', 'frame_number', 'brand_ocr_asset', 'object_x_centre', 'object_y_centre', 'object_width',
            'object_height', 'percentage_probability', 'pts_time')

class DetectedAudioCalloutsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetectedAudioCallouts
        fields = (
            'id', 'event', 'sentence_start_time', 'sentence', 'confidence', 'word_list'
        )

    def to_representation(self, instance):
        change_fields = ('word_list',)
        data = super().to_representation(instance)
        for field in change_fields:
            try:
                if not data[field]:
                    data[field] = []
            except KeyError:
                pass
        return data