from rest_framework import routers
from django.urls import path, include
from events.views import (
    DetectedObjectsViewSet, DetectedPromotionalObjectsViewSet,
    DetectedOCRObjectsViewSet, BroadcasterViewSet,
    DetectedAudioCalloutsViewSet, EventViewSet,
    InProgressEventViewSet, CompletedEventViewSet,
    BigEventVideoViewSet
)

router = routers.DefaultRouter()
router.register(r'broadcasters', BroadcasterViewSet)
router.register(r'events', EventViewSet)
router.register(r'big_event_videos', BigEventVideoViewSet)
router.register(r'in_progress_events', InProgressEventViewSet)
router.register(r'completed_events', CompletedEventViewSet)
# router.register(r'event_videos', ListEventVideoView)
router.register(r'detected_objects', DetectedObjectsViewSet)
router.register(r'detected_promotional_objects', DetectedPromotionalObjectsViewSet)
router.register(r'detected_ocr_objects', DetectedOCRObjectsViewSet)
router.register(r'detected_audio_callouts', DetectedAudioCalloutsViewSet)

urlpatterns = [
    path('', include(router.urls))
]
