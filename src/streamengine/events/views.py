from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404

from events.models import (
    DetectedObjects, DetectedPromotionalObjects,
    DetectedOCRObjects, Broadcaster,
    DetectedAudioCallouts, Event, BigVideo
)
from events.serializers import (
    DetectedObjectsSerializer, DetectedPromotionalObjectsSerializer,
    DetectedOCRObjectsSerializer, BroadcasterSerializer,
    DetectedAudioCalloutsSerializer, EventSerializer,
    BigEventVideoSerializer

)
from events.mixins.rest import CreateListMixin
from rest_framework import permissions
from rest_framework import generics


class BroadcasterViewSet(viewsets.ModelViewSet):
    queryset = Broadcaster.objects.all()
    serializer_class = BroadcasterSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.filter(event_status="upcoming")
    serializer_class = EventSerializer

class InProgressEventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.filter(event_status="in_progress")
    serializer_class = EventSerializer


class CompletedEventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.filter(event_status="completed")
    serializer_class = EventSerializer


class BigEventVideoViewSet(viewsets.ModelViewSet):
    serializer_class = BigEventVideoSerializer
    queryset = BigVideo.objects.all()


class DetectedObjectsViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = DetectedObjects.objects.all()
    serializer_class = DetectedObjectsSerializer

class DetectedPromotionalObjectsViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = DetectedPromotionalObjects.objects.all()
    serializer_class = DetectedPromotionalObjectsSerializer

class DetectedOCRObjectsViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = DetectedOCRObjects.objects.all()
    serializer_class = DetectedOCRObjectsSerializer

class DetectedAudioCalloutsViewSet(CreateListMixin, viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = DetectedAudioCallouts.objects.all()
    serializer_class = DetectedAudioCalloutsSerializer